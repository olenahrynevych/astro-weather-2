package com.example.astro1;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Date;

public class ForecastWeatherFragment extends Fragment {

    TextView date1;
    TextView lowTemp1;
    TextView highTemp1;
    TextView desc1;

    TextView date2;
    TextView lowTemp2;
    TextView highTemp2;
    TextView desc2;

    TextView date3;
    TextView lowTemp3;
    TextView highTemp3;
    TextView desc3;

    TextView date4;
    TextView lowTemp4;
    TextView highTemp4;
    TextView desc4;

    TextView date5;
    TextView lowTemp5;
    TextView highTemp5;
    TextView desc5;

    public ForecastWeatherFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_forecast_weather, container, false);
        setParams(view);
        getWeatherInfo();
        return view;
    }


    public void getWeatherInfo() {
        try {
            WeatherModel weatherModel = ((ForecastActivity) getActivity()).weatherModel;
            String dataFormat = ((ForecastActivity) getActivity()).format;

            setParamsWithDatafromApi(weatherModel, dataFormat);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    private void setParams(View view) {
        date1 = view.findViewById(R.id.date1);
        lowTemp1 = view.findViewById(R.id.lowTemp1);
        highTemp1 = view.findViewById(R.id.highTemp1);
        desc1 = view.findViewById(R.id.desc1);

        date2 = view.findViewById(R.id.date2);
        lowTemp2 = view.findViewById(R.id.lowTemp2);
        highTemp2 = view.findViewById(R.id.highTemp2);
        desc2 = view.findViewById(R.id.desc2);

        date3 = view.findViewById(R.id.date3);
        lowTemp3 = view.findViewById(R.id.lowTemp3);
        highTemp3 = view.findViewById(R.id.highTemp3);
        desc3 = view.findViewById(R.id.desc3);

        date4 = view.findViewById(R.id.date4);
        lowTemp4 = view.findViewById(R.id.lowTemp4);
        highTemp4 = view.findViewById(R.id.highTemp4);
        desc4 = view.findViewById(R.id.desc4);

        date5 = view.findViewById(R.id.date5);
        lowTemp5 = view.findViewById(R.id.lowTemp5);
        highTemp5 = view.findViewById(R.id.highTemp5);
        desc5 = view.findViewById(R.id.desc5);
    }

    private void setParamsWithDatafromApi(WeatherModel forecastDataModel, String dataFormat) {
        date1.setText(convertUnixToDate(forecastDataModel.forecasts[0].date));
        lowTemp1.setText(forecastDataModel.forecasts[0].low + (dataFormat.isEmpty() ? " F" : " C"));
        highTemp1.setText(forecastDataModel.forecasts[0].high + (dataFormat.isEmpty() ? " F" : " C"));
        desc1.setText(forecastDataModel.forecasts[0].text);

        date2.setText(convertUnixToDate(forecastDataModel.forecasts[1].date));
        lowTemp2.setText(forecastDataModel.forecasts[1].low + (dataFormat.isEmpty() ? " F" : " C"));
        highTemp2.setText(forecastDataModel.forecasts[1].high + (dataFormat.isEmpty() ? " F" : " C"));
        desc2.setText(forecastDataModel.forecasts[1].text);

        date3.setText(convertUnixToDate(forecastDataModel.forecasts[2].date));
        lowTemp3.setText(forecastDataModel.forecasts[2].low + (dataFormat.isEmpty() ? " F" : " C"));
        highTemp3.setText(forecastDataModel.forecasts[2].high + (dataFormat.isEmpty() ? " F" : " C"));
        desc3.setText(forecastDataModel.forecasts[2].text);

        date4.setText(convertUnixToDate(forecastDataModel.forecasts[3].date));
        lowTemp4.setText(forecastDataModel.forecasts[3].low + (dataFormat.isEmpty() ? " F" : " C"));
        highTemp4.setText(forecastDataModel.forecasts[3].high + (dataFormat.isEmpty() ? " F" : " C"));
        desc4.setText(forecastDataModel.forecasts[3].text);

        date5.setText(convertUnixToDate(forecastDataModel.forecasts[4].date));
        lowTemp5.setText(forecastDataModel.forecasts[4].low + (dataFormat.isEmpty() ? " F" : " C"));
        highTemp5.setText(forecastDataModel.forecasts[4].high + (dataFormat.isEmpty() ? " F" : " C"));
        desc5.setText(forecastDataModel.forecasts[4].text);
    }

    private static String convertUnixToDate(String dt) {
        long parsed = Long.parseLong(dt);
        Date date = new Date(parsed * 1000L);
        SimpleDateFormat sfd = new SimpleDateFormat("EEE, MMM dd");
        String formatted = sfd.format(date);
        return formatted;
    }
}
