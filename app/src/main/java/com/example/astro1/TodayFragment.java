package com.example.astro1;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Date;


/**
 * A simple {@link Fragment} subclass.
 */
public class TodayFragment extends Fragment {


    TextView txt_city_name;
    ImageView img_weather;
    TextView txt_city;
    TextView txt_pressure;
    TextView txt_humidity;
    TextView txt_sunrise;
    TextView txt_sunset;
    TextView txt_temperature;
    TextView txt_date_time;
    TextView txt_wind;
    TextView txt_geo_coords;

    LinearLayout weather_panel;

    public TodayFragment() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_today, container, false);
        setParams(view);
        getWeatherInfo();
        return view;
    }


    public void getWeatherInfo() {

        try {
            WeatherModel weatherModel = ((ForecastActivity) getActivity()).weatherModel;
            String dataFormat = ((ForecastActivity) getActivity()).format;

            if (weatherModel != null) {
                setParamsWithDatafromApi(weatherModel, dataFormat);

            }

            weather_panel.setVisibility(View.VISIBLE);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void setParams(View view) {
        txt_city_name = view.findViewById(R.id.txt_city_name);
        img_weather = view.findViewById(R.id.img_weather);
        txt_city = view.findViewById(R.id.txt_city_name);
        txt_date_time = view.findViewById(R.id.txt_date_time);
        txt_geo_coords = view.findViewById(R.id.txt_coords);
        txt_humidity = view.findViewById(R.id.txt_humidity);
        txt_pressure = view.findViewById(R.id.txt_pressure);

        txt_sunrise = view.findViewById(R.id.txt_sunrise);
        txt_sunset = view.findViewById(R.id.txt_sunset);
        txt_temperature = view.findViewById(R.id.txt_temperature);
        txt_wind = view.findViewById(R.id.txt_wind);
        weather_panel = view.findViewById(R.id.weather_panel);
    }

    private void setParamsWithDatafromApi(WeatherModel forecastDataModel, String dataFormat) {

        txt_city.setText("Weather in " + forecastDataModel.location.city + ", " + forecastDataModel.location.region + ", " + forecastDataModel.location.country);
        txt_date_time.setText(convertUnixToDate(forecastDataModel.current_observation.pubDate));
        txt_temperature.setText(forecastDataModel.current_observation.condition.temperature + (dataFormat.isEmpty() ? " F" : " C"));
        txt_wind.setText(forecastDataModel.current_observation.wind.speed + (dataFormat.isEmpty() ? " m/h" : " km/h"));
        txt_pressure.setText(forecastDataModel.current_observation.atmosphere.pressure + (dataFormat.isEmpty() ? " inchHg" : " mbar"));
        txt_humidity.setText(forecastDataModel.current_observation.atmosphere.humidity + "%");
        txt_sunrise.setText(forecastDataModel.current_observation.astronomy.sunrise);
        txt_sunset.setText(forecastDataModel.current_observation.astronomy.sunset);
        txt_geo_coords.setText(forecastDataModel.location.latitude + "° , " + forecastDataModel.location.longitude + "°");
    }

    private static String convertUnixToDate(String dt) {
        long parsed = Long.parseLong(dt);
        Date date = new Date(parsed * 1000L);
        SimpleDateFormat sfd = new SimpleDateFormat("EEE, MMM dd");
        String formatted = sfd.format(date);
        return formatted;
    }

    @Override
    public void onStop() {
        super.onStop();
    }

}
