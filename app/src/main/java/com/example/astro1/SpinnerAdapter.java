package com.example.astro1;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class SpinnerAdapter extends BaseAdapter {
    Activity activity;
    String[] minutsToRefresh;
    LayoutInflater layoutInflater;

    public SpinnerAdapter(Activity activity, String[] minutsToRefresh, LayoutInflater layoutInflater) {
        this.activity = activity;
        this.minutsToRefresh = minutsToRefresh;
        this.layoutInflater = layoutInflater;
    }

    @Override
    public int getCount() {
         return minutsToRefresh.length;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = layoutInflater.inflate(R.layout.spinner_row, null);
        TextView textView = row.findViewById(R.id.title);
        textView.setText(minutsToRefresh[position]);
        return row;
    }
}
