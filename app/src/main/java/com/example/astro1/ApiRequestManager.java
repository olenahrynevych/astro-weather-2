package com.example.astro1;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

public class ApiRequestManager {
    private static ApiRequestManager requestManager;

    Context context;
    RequestQueue requestQueue;

    public static synchronized ApiRequestManager getInstance(Context context) {
        if (requestManager == null) {
            requestManager = new ApiRequestManager(context);
        }
        return requestManager;
    }

    private ApiRequestManager(Context context) {
        this.context = context;
        requestQueue = Volley.newRequestQueue(this.context);
    }

    public <T> void addToRequestQueue(Request<T> request) {
        requestQueue.add(request);
    }
}