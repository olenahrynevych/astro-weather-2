package com.example.astro1;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class AddLocalization extends AppCompatActivity {

    Button addLocalization;
    EditText userLocalization;
    LinearLayout buttonList;
    String[] localozationsList = new String[100];
    String jsonData;
    String localization;

    WeatherModel weatherModel;
    boolean exists;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_localization);

        buttonList = findViewById(R.id.buttonList);
        readData();
        addLocalization = findViewById(R.id.addLocalization);
        userLocalization = findViewById(R.id.editLocalization);


        addLocalization.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                localization = userLocalization.getText().toString();

                if (cityExists(localization)) {
                    final Button button = new Button(getApplicationContext());
                    button.setText(localization);
                    button.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent intent = new Intent(AddLocalization.this, ForecastActivity.class);
                            intent.putExtra("localizationInfo", button.getText().toString().replaceAll("\\s+", ""));
                            startActivity(intent);
                        }
                    });
                    saveData(button);
                    buttonList.addView(button);
                }
            }
        });

    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putString("localization", localization);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    private void readData() {
        SharedPreferences sharedPreferences = getSharedPreferences("savedData", MODE_PRIVATE);
        jsonData = sharedPreferences.getString("savedData", "");
        if (!jsonData.isEmpty()) {
            GsonBuilder gsonBuilder = new GsonBuilder();
            gsonBuilder.setDateFormat("M/d/yy hh:mm a");
            Gson gson = gsonBuilder.create();
            localozationsList = gson.fromJson(jsonData, String[].class);
        }

        for (int i = 0; i < localozationsList.length; i++) {
            if (localozationsList[i] != null && !localozationsList[i].isEmpty()) {
                final Button button = new Button(getApplicationContext());
                button.setText(localozationsList[i]);
                button.setTextColor(Color.BLACK);
                button.setBackgroundColor(Color.WHITE);
                button.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(AddLocalization.this, ForecastActivity.class);
                        intent.putExtra("localizationInfo", button.getText().toString().replaceAll("\\s+", ""));
                        startActivity(intent);
                    }
                });
                buttonList.addView(button);
            } else
                break;
        }
    }

    private void saveData(Button button) {
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.setDateFormat("M/d/yy hh:mm a");
        Gson gson = gsonBuilder.create();
        for (int i = 0; i < localozationsList.length; i++)
            if (localozationsList[i] == null || localozationsList[i].isEmpty()) {
                localozationsList[i] = button.getText().toString();
                break;
            }
        jsonData = gson.toJson(localozationsList);
        SharedPreferences sharedPreferences = getSharedPreferences("savedData", MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("savedData", jsonData);
        editor.commit();
    }

    private boolean cityExists(String localization) {

        weatherModel = null;
        try {
            ApiRequest request = new ApiRequest(Request.Method.GET, null, null, new Response.Listener() {
                @Override
                public void onResponse(Object response) {
                    GsonBuilder gsonBuilder = new GsonBuilder();
                    gsonBuilder.setDateFormat("M/d/yy hh:mm a");
                    Gson gson = gsonBuilder.create();
                    String jsonString = response.toString();
                    weatherModel = gson.fromJson(jsonString, WeatherModel.class);
                    if (weatherModel.location.woeid != null) exists = true;
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    exists = false;
                }
            });

            ApiRequestManager requestManager = ApiRequestManager.getInstance(getApplicationContext());
            request.setCity(localization);
            requestManager.addToRequestQueue(request);
        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), "Not found", Toast.LENGTH_LONG).show();
            exists=false;
        }

        return exists;
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        localization = savedInstanceState.getString("localization");
        super.onRestoreInstanceState(savedInstanceState);
    }
}
