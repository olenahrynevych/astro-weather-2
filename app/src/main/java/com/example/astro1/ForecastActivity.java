package com.example.astro1;

import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class ForecastActivity extends AppCompatActivity {


    String localizationName;

    ViewPager pager;
    PagerAdapter pagerAdapter;

    Button refreshData;
    Button imperialSystem;
    Button metricSystem;
    WeatherModel weatherModel;

    String format = "";

    FragmentManager fragmentManager;
    FragmentTransaction fragmentTransaction;

    TodayFragment todayFragment = new TodayFragment();
    ForecastWeatherFragment forecastWeatherFragment = new ForecastWeatherFragment();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forecast);
        localizationName = getIntent().getStringExtra("localizationInfo");

        refreshData = findViewById(R.id.refresh);
        refreshData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateFragments();
            }
        });

        imperialSystem = findViewById(R.id.imperialSystem);
        imperialSystem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                format = "";
                getDatafromAPi(false);
            }
        });

        metricSystem = findViewById(R.id.metricSystem);
        metricSystem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                format = "&u=c";
                getDatafromAPi(false);
            }
        });

        boolean isTablet = (getResources().getConfiguration().screenLayout
                & Configuration.SCREENLAYOUT_SIZE_MASK)
                >= Configuration.SCREENLAYOUT_SIZE_LARGE;

        if (isTablet && savedInstanceState == null) {
            fragmentManager = getSupportFragmentManager();
            fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.add(R.id.forecastContent, forecastWeatherFragment, "forecastWeatherFragment");
            fragmentTransaction.add(R.id.todayContent, todayFragment, "todayFragment");
            fragmentTransaction.commit();
        } else if (isTablet && savedInstanceState != null) {
            fragmentManager = getSupportFragmentManager();
            fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.forecastContent, forecastWeatherFragment, "forecastWeatherFragment");
            fragmentTransaction.replace(R.id.todayContent, todayFragment, "todayFragment");
            fragmentTransaction.commit();
        } else {
            pager = (ViewPager) findViewById(R.id.forecastContainer);
            pagerAdapter = new ListPagerAdapter(getSupportFragmentManager());
            pager.setAdapter(pagerAdapter);
        }
        getDatafromAPi(true);

    }

    public void getDatafromAPi(final boolean fromButton) {
        ApiRequest request = new ApiRequest(Request.Method.GET, null, null, new Response.Listener() {
            @Override
            public void onResponse(Object response) {
                SharedPreferences sharedPreferences = getSharedPreferences(localizationName, MODE_PRIVATE);
                updateSavedModel(response, sharedPreferences);
                updateFragments();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (fromButton)
                    Toast.makeText(getApplicationContext(), "Data can be not actual,turn on internet...", Toast.LENGTH_LONG).show();
                getSavedModel();
                updateFragments();
            }
        });
        ApiRequestManager requestManager = ApiRequestManager.getInstance(this);
        request.setCity(localizationName);
        request.setDataFormat(format);
        requestManager.addToRequestQueue(request);
    }

    private void getSavedModel() {
        SharedPreferences sharedPreferences = getSharedPreferences(localizationName, MODE_PRIVATE);
        String jsonString = sharedPreferences.getString(localizationName, "");
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.setDateFormat("M/d/yy hh:mm a");
        Gson gson = gsonBuilder.create();
        weatherModel = gson.fromJson(jsonString, WeatherModel.class);
    }
    private void updateSavedModel(Object response, SharedPreferences sharedPreferences) {
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.setDateFormat("M/d/yy hh:mm a");
        Gson gson = gsonBuilder.create();
        String jsonString = response.toString();
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(localizationName, jsonString);
        editor.commit();
        weatherModel = gson.fromJson(jsonString, WeatherModel.class);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("format", format);

    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        format = savedInstanceState.getString("format");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    public void updateFragments() {
        todayFragment.getWeatherInfo();
        forecastWeatherFragment.getWeatherInfo();
    }


    class ListPagerAdapter extends PagerAdapter {

        FragmentManager fragmentManager;
        Fragment[] fragments;

        ListPagerAdapter(FragmentManager fm) {
            fragmentManager = fm;
            fragments = new Fragment[2];
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            assert (0 <= position && position < fragments.length);
            FragmentTransaction trans = fragmentManager.beginTransaction();
            trans.remove(fragments[position]);
            trans.commit();
            fragments[position] = null;
        }

        @Override
        public Fragment instantiateItem(ViewGroup container, int position) {
            Fragment fragment = getItem(position);
            FragmentTransaction trans = fragmentManager.beginTransaction();
            trans.add(container.getId(), fragment, "fragment:" + position);
            trans.commit();
            return fragment;
        }

        @Override
        public int getCount() {
            return fragments.length;
        }

        @Override
        public boolean isViewFromObject(View view, Object fragment) {
            return ((Fragment) fragment).getView() == view;
        }

        public Fragment getItem(int position) {
            assert (0 <= position && position < fragments.length);
            if (fragments[position] == null) {
                if (position == 0)
                    fragments[position] = (todayFragment = new TodayFragment());
                else if (position == 1)
                    fragments[position] = (forecastWeatherFragment = new ForecastWeatherFragment());

            }
            return fragments[position];
        }
    }
}
